@extends('layouts.app')

@section('content')



    <div class="slider">
        <div class="callbacks_container">
            <ul class="rslides" id="slider">
                <li>
                    <img src="http://placehold.it/1420x560" alt="">

                </li>
                <li>
                    <img src="http://placehold.it/1420x560" alt="">

                </li>
                <li>
                    <img src="http://placehold.it/1420x560" alt="">

                </li>
            </ul>

        </div>
    </div>
    <!---->
    <div class="content">
        <div class="container">
            <div class="content-top">
                <div class="content-top1">

                    <div class=" col-md-4 grid-top">
                        <div class="thumbnail">
                            <div class="grid-at">
                                <i class="fa fa-rocket fa-icon"></i>
                                <h3 class="grid">Our Mission</h3>
                                <div class="clearfix"></div>
                            </div>
                            <p> Our mission is to provide technical, material and financial support to the community
                                with the aim of
                                alleviating suffering, caused by the conflicts of groups or natural disasters such as
                                HIV/AIDS and poverty. </p>

                        </div>
                    </div>
                    <div class=" col-md-4 grid-top">
                        <div class="thumbnail">
                            <div class="grid-at">
                                <i class="fa fa-binoculars fa-icon"></i>
                                <h3 class="grid">Our Vision</h3>
                                <div class="clearfix"></div>
                            </div>
                            <p> We are focusing on alleviating human sufferings, human-centered development,
                                participatory approach in development process, resources and project sustainability at
                                all levels in the community.</p>

                        </div>
                    </div>
                    <div class=" col-md-4 grid-top">
                        <div class="thumbnail">
                            <div class="grid-at">
                                <i class="fa fa-crosshairs fa-icon"></i>
                                <h3 class="grid">Our Goal</h3>
                                <div class="clearfix"></div>
                            </div>
                            <p>We promote & support orphans and vulnerable children to improve their
                                livelihood through implementation of various economic activities with care of their
                                health and improving education for the purpose of creating a peaceful society. </p>

                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="content-bottom donate">

                <div class="col-md-6 red">
                    <a href="single.html"><img class="img-responsive" src="http://placehold.it/580x355" alt=""></a>
                </div>
                <div class="col-md-6 come">
                    <div class=" welcome">
                        <h3>We need your help</h3>
                        <p class="for">Join us to help the children</p>
                        <p>We have no a permanent donor at this infancy stage. Without a reliable source of donor funds,
                            it is not possible for us to achieve out goals. We request that you provide us financial and
                            technical assistance in order to strengthen our organization in working with and support
                            these unfortunate children who through no fault of their own are suffering from the impact
                            of HIV/AIDS.</p>
                        <a class="hvr-rectangle-out" data-toggle="modal" href='#modal-id'>Donate</a>
                        <div class="modal fade" id="modal-id">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">How to donate</h4>
                                    </div>
                                    <div class="modal-body">
                                        <dl class="dl-horizontal">
                                            <dt>MPESA</dt>
                                            <dd>0000 00 00 00</dd>
                                        </dl>
                                        <hr>
                                        <dl class="dl-horizontal">
                                            <dt>Airtel money</dt>
                                            <dd>0000 00 00 00</dd>
                                        </dl>
                                        <hr>
                                        <dl class="dl-horizontal">
                                            <dt>Tigo Pesa</dt>
                                            <dd>0000 00 00 00</dd>
                                        </dl>
                                        <hr>
                                        <dl class="dl-horizontal">
                                            <dt>Halotel Pesa</dt>
                                            <dd>0000 00 00 00</dd>
                                        </dl>
                                        <hr>
                                        <dl class="dl-horizontal">
                                            <dt>Account number:</dt>
                                            <dd>5900427001</dd>
                                            <dt>Bank name</dt>
                                            <dd>Diamond Trust Bank</dd>
                                            <dt>Swift Code</dt>
                                            <dd>DTKETZTZ</dd>
                                        </dl>
                                        <hr>
                                        <dl class="dl-horizontal">
                                            <dt>Paypal</dt>
                                            <dd><a href="#" class="btn btn-primary"><i class="fa fa-paypal"></i></a>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="content-use">
                <div class=" content-use-bottom">
                    <h3>Our Works</h3>
                    <div class="our">
                        <div class="col-md-4 our-at">
                            <a href="#"><img class="img-responsive" src="http://placehold.it/440x174" alt=""></a>
                            <h4><a href="#">Zam-Zam Orphanage Center</a></h4>
                            <p>Zam-Zam Orphanage Center is situated at Miyuji. We help the children who live in hard and
                                difficult environment who do not have access to food, shelter, clothes and other
                                guidance support in order to grow with moral values.</p>
                            <a class="hvr-rectangle-out" href="#">Read More</a>
                        </div>
                        <div class="col-md-4 our-at">
                            <a href="#"><img class="img-responsive" src="http://placehold.it/440x174" alt=""></a>
                            <h4><a href="#">Zam-Zam Primary School</a></h4>
                            <p>Zam-Zam Primary School is situated at Miyuji with more than 56 children. We provide
                                primary education to give the children a strong foundation for their lives. We also have
                                kindergarten school situated at Area A which has more than 67 children.</p>
                            <a class="hvr-rectangle-out" href="#">Read More</a>
                        </div>
                        <div class="col-md-4 our-at">
                            <a href="#"><img class="img-responsive" src="http://placehold.it/440x174" alt=""></a>
                            <h4><a href="#">Zam-Zam Secondary School</a></h4>
                            <p>Zam-Zam Secondary School is situated at Kondoa District in Dodoma region. We provide
                                secondary school education to give the children the necessary knowledge to contribute to
                                the development of the nation.</p>
                            <a class="hvr-rectangle-out" href="#">Read More</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <!---->
        <div class="col-mn">
            <div class="container">

                <div class="col-mn2">
                    <h3>core values</h3>
                    <p>Creativity, Integration, Cooperation, Decision making, Discipline, Equity/Equality and
                        Transparency.</p>

                    <a class="hvr-rectangle-out more-in" href="#">Read More</a>

                </div>
            </div>
        </div>
        <!---->
        <div class="container">
            <div class="content-mid">
                <h3> Events</h3>
                <div class="news">
                    <div class="col-md-6 new-more">
                        <div class="six">
                            <h4>26<span>June</span></h4>
                            <h6><a href="#">Opening of Zam-Zam Pre & Primary School </a></h6>
                            <div class="clearfix"></div>
                        </div>
                        <p>Kasertas lertyasea deeraeser miasera lertasa ritise doloert ferdas caplicabo nerafaes asety u
                            lasec vaserat. nikertyade asetkertyptaiades goertayse.nikertyade asetkertyptaiades
                            goertayse</p>
                        <a class="hvr-rectangle-out" href="#">Read More</a>
                    </div>
                    <div class="col-md-6 new-more">
                        <div class="six">
                            <h4>26<span>April</span></h4>
                            <h6><a href="#">Graduation ceremony for standard seven </a></h6>
                            <div class="clearfix"></div>
                        </div>
                        <p>Kasertas lertyasea deeraeser miasera lertasa ritise doloert ferdas caplicabo nerafaes asety u
                            lasec vaserat. nikertyade asetkertyptaiades goertayse.nikertyade asetkertyptaiades
                            goertayse</p>
                        <a class="hvr-rectangle-out" href="#">Read More</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
        <!---->

    </div>


@endsection    
 