@extends('layouts.app')

@section('content')

    <div class="contact">
        <div class="container">

            <h2>Contact</h2>
            <form action="contact" method="POST">

                {!! csrf_field() !!}

                <div class="contact-grid">
                    <div class="col-md-6 contact-us">
                        <input type="text" name="name" value="Name" onfocus="this.value='';"
                               onblur="if (this.value == '') {this.value = 'Name';}">
                    </div>
                    <div class="col-md-6 contact-us">
                        <input type="text" name="email" value="Email-id" onfocus="this.value='';"
                               onblur="if (this.value == '') {this.value = 'Email-id';}">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="contact-grid">
                    <div class="col-md-6 contact-us">
                        <input type="text" name="url" value="URL" onfocus="this.value='';"
                               onblur="if (this.value == '') {this.value = 'URL';}">
                    </div>
                    <div class="col-md-6 contact-us">
                        <input type="text" name="subject" value="Subject" onfocus="this.value='';"
                               onblur="if (this.value == '') {this.value = 'Subject';}">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <textarea name="message" cols="77" rows="6" value=" " onfocus="this.value='';"
                          onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                <div class="send ">
                    <input type="submit" value="SEND">
                </div>
            </form>
        </div>

        <div class="container map">       
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCp1bawGjAxGmz223KoJ9nL-tnsPYC3tWE"></script><div style="overflow:hidden;height:500px;width:100%;"><div id="gmap_canvas" style="height:500px;width:100%;"><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.map-embed.com" id="get-map-data">www.map-embed.com</a></div><script type="text/javascript"> function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(-6.162959000000001,35.75160690000007),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(-6.162959000000001, 35.75160690000007)});infowindow = new google.maps.InfoWindow({content:"<b>Dalai Islamic Center</b><br/>CDA Building<br/> Dodoma" });google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>            
        </div>

    </div>

@endsection
