@extends('layouts.app')

@section('content')
    <div class="about">
        <div class="container">
            <div class="about-left">
                <h2>About</h2>
                <div class="about-bottom">
                    <div class="col-md-7 about-grid">
                        <h6><a href="">Brief description of the Organization.</a></h6>
                        <p>DIC is an innovative Faith based organization registered for working in any part of Tanzania.Its head office
                        is in Dodoma Region.Central part of Tanzania.Currently the organization is mostly working in Dodoma Region.</p>

                        <h6><a href="">Mission.</a></h6>
                        <p>To provide technical, materials and financial support to the community with the aim of alleviating suffering,caused
                            by the conflicts of groups or natural disasters such as HIV/AIDS and poverty.</p>

                        <h6><a href="">Vision.</a></h6>
                        <p>Focus on: alleviating human suffering: Human-centered development; participatory approach in development process,
                            resources and project sustainability at all levels in the community.</p>

                        <h6><a href="">Goals</a></h6>
                        <p>The main goal of DIC is to promote and support orphans and vulnerable children to improve their livelihood through
                            implementation of various economic activities with care of their health status and improving education for the purpose
                            of creating awareness against different calamities and diseases.</p>
                    </div>
                    <div class="col-md-5 about-grid1">
                        <a href="" >
                            <img src="http://placehold.it/500x315" alt="" class="img-responsive ">
                        </a>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!---->
        <div class=" in-profile">
            <div class="container">
                <h3> Team</h3>
                <div class="team-left ">
                    <div class="col-md-4 team-top">
                        <a href="single.html"><img class="img-responsive" src="http://placehold.it/280x250" alt=""></a>
                        <h6>RASHID BURA</h6>
                        <p>General secretary .</p>
                        <p>Tel 0715 305 310/0754 305 310/0785 305 310</p>
                    </div>
                    <div class="col-md-4 team-top ">
                        <a href="single.html"><img class="img-responsive  " src="http://placehold.it/280x250" alt=""></a>
                        <h6>MASHAKA MNJALO</h6>
                        <p>Techical Advisor</p><p>(B.counceling Tumaini University)</p>
                        <p>Tel 0767 357 424/0715 357 424/0784 357 424</p>
                    </div>
                    <div class="col-md-4 team-top">
                        <a href="single.html"><img class="img-responsive  " src="http://placehold.it/280x250" alt=""></a>
                        <h6>MAULIDI RASHIDI</h6>
                        <p>Techical Advisor</p><p>(B.Science Education St.John's University)</p>
                        <p>Tel.0758 414 107</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!---->
        <div class="container">
            <div class="about-top">
                <h3>A Few Words About Us</h3>
                <div class="col-md1 ">
                    <div class="col-md-6 few">
                        <div class="col-md-6 about-grid2">
                            <a href="single.html" >
                                <img src="http://placehold.it/300x226" alt="" class="img-responsive ">
                            </a>
                        </div>
                        <div class="col-md-6 about-grid3">
                            <h6><a href="single.html">single.html">Brief description of the Organization</a></h6>
                            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas tristique orci ac sem. Duis ultricies pharetra magna duis ultricies </p>
                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="col-md-6 few">
                        <div class="col-md-6 about-grid2">
                            <a href="single.html" >
                                <img src="http://placehold.it/300x226" alt="" class="img-responsive ">
                            </a>
                        </div>
                        <div class="col-md-6 about-grid3">
                            <h6><a href="single.html">Brief description of the Organization</a></h6>
                            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas tristique orci ac sem. Duis ultricies pharetra magna duis ultricies </p>
                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md1 ">
                    <div class="col-md-6 few">
                        <div class="col-md-6 about-grid2">
                            <a href="single.html" >
                                <img src="http://placehold.it/300x226" alt="" class="img-responsive ">
                            </a>
                        </div>
                        <div class="col-md-6 about-grid3">
                            <h6><a href="single.html">Maecenas tristique orci ac sem Duis ultricies  </a></h6>
                            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas tristique orci ac sem. Duis ultricies pharetra magna duis ultricies </p>
                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="col-md-6 few">
                        <div class="col-md-6 about-grid2">
                            <a href="single.html" >
                                <img src="http://placehold.it/300x226" alt="" class="img-responsive ">
                            </a>
                        </div>
                        <div class="col-md-6 about-grid3">
                            <h6><a href="single.html">Maecenas tristique orci ac sem Duis ultricies  </a></h6>
                            <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Maecenas tristique orci ac sem. Duis ultricies pharetra magna duis ultricies</p>
                        </div>

                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!---->
    </div>
@endsection
