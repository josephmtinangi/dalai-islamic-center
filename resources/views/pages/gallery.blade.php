@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="text-center">Photo Gallery</h2>
        <hr>
        <div class="row">
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 1">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 2">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 3">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 4">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 1">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 2">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 3">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 4">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 1">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 2">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 3">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
            <div class="col-sm-3">
                <a href="http://placehold.it/400x250" data-lightbox="image-1" data-title="Caption 4">
                    <img src="http://placehold.it/400x250" class="img-responsive" alt="">
                </a>
            </div>
        </div>
    </div>
@endsection
