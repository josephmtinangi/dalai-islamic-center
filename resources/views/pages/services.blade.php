@extends('layouts.app')

@section('content')

    <div class="services">
        <div class="container">
            <h2>Objectives</h2>
            <div class="service-grid">
                <h3>How We Work</h3>
                <div class="work">
                    <div class="col-md-4 grid-service">
                        <a href="#"><img class="img-responsive" src="http://placehold.it/400x250" alt=" "></a>
                        <h4><a href="#">Awareness creation </a></h4>
                        <p>We create awareness of the environment and encourage the public to cares for the needs of
                            orphans and vulnerable children disabled and widows in the community.</p>

                    </div>
                    <div class="col-md-4 grid-service">
                        <a href="#"><img class="img-responsive" src="http://placehold.it/400x250" alt=" "></a>
                        <h4><a href="#">Educate the community </a></h4>
                        <p>We help and educate the community on the meaning of human development promoted by Islamic
                            teaching.</p>

                    </div>
                    <div class="col-md-4 grid-service">
                        <a href="#"><img class="img-responsive" src="http://placehold.it/400x250" alt=" "></a>
                        <h4><a href="#">Collaboration </a></h4>
                        <p>We collaborate with the government, community members, national and international NGOs in
                            fighting against the spread and transmission of HIV/AIDS to children and poverty alleviation
                            in Tanzania. </p>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="work">
                    <div class="col-md-4 grid-service">
                        <a href="#"><img class="img-responsive" src="http://placehold.it/400x250" alt=" "></a>
                        <h4><a href="#">Community development </a></h4>
                        <p>We establish and build schools, orphanage centers, health centers, clinics and hospitals for
                            the community services, widows and disabled.</p>

                    </div>
                    <div class="col-md-4 grid-service">
                        <a href="#"><img class="img-responsive" src="http://placehold.it/400x250" alt=" "></a>
                        <h4><a href="#">Peace promotion </a></h4>
                        <p>We promote peace making initiatives, reconciliation and crisis intervention among the
                            community in collaboration with the government and other faith groups.</p>

                    </div>
                    <div class="col-md-4 grid-service">
                        <a href="#"><img class="img-responsive" src="http://placehold.it/400x250" alt=" "></a>
                        <h4><a href="#">Poverty alleviation </a></h4>
                        <p>We collaborate with the government, community members, national and international NGOs in
                            fighting against the spread and transmission of HIV/AIDS to children and poverty alleviation
                            in Tanzania. </p>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="col-mn">
            <div class="container">

                <div class="col-mn2">
                    <h3>the best features</h3>
                    <p>Contrary to popular belief
                        , Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature
                        from 45 BC, making it over 2000 years old.</p>

                    <a class="hvr-rectangle-out more-in" href="#">Read More</a>

                </div>
            </div>
        </div>
        <div class="container">
            <div class="service-bottom">
                <div class="col-md-8 service-top">
                    <h3>Our specific objectives</h3>
                    <div class=" service-top-bottom">
                        <div class="col-md-6 ser-in">
                            <i class="fa fa-hand-spock-o fa-icon"> </i>
                            <div class="service-on">
                                <h4><a href="#">Support orphans </a></h4>
                                <p>To support orphans and vulnerable children (Islamic society) affected by HIV/AIDS
                                    (Widows and disabled) with psycho education and economic gain for improving their
                                    livelihood in the community. </p>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6 ser-in">
                            <i class="fa fa-heartbeat fa-icon"> </i>
                            <div class="service-on">
                                <h4><a href="#">Improve Health </a></h4>
                                <p>To improve health status of orphans and vulnerable children through heath care
                                    awareness and counseling services. </p>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class=" service-top-bottom">
                        <div class="col-md-6 ser-in">
                            <i class="fa fa-money fa-icon"> </i>
                            <div class="service-on">
                                <h4><a href="#">Income generation </a></h4>
                                <p>To support income generation project through micro-credit and economic gain project
                                    from different donor agencies. </p>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-6 ser-in">
                            <i class="fa fa-book fa-icon"> </i>
                            <div class="service-on">
                                <h4><a href="#">Give reproductive health education </a></h4>
                                <p>To give education of reproductive health and combat negative culture like female
                                    genital mutilation to women and domestic abuse in household. </p>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4 service-top-at">
                    <h3>Projects we have conducted</h3>
                    <ul class="fa-ul">
                        <li><i class="fa-li fa fa-check-square"></i> AIDS prevention to the religious
                            leaders in Dodoma manicipality and Kongwa district in Dodoma Tanzania. <a href="#">Read
                                more &raquo;</a>
                        </li>
                        <li><i class="fa-li fa fa-check-square"></i> HIV/AIDS support project on AIDS orphans basic
                            education initiative in Dodoma municipality. <a href="#">Read more &raquo;</a>
                        </li>
                        <li><i class="fa-li fa fa-check-square"></i> Vocational training courses <a href="#">Read
                                more &raquo;</a></li>
                        <li><i class="fa-li fa fa-check-square"></i> Building primary and secondary schools for
                            children.
                            <a href="#">Read more &raquo;</a></li>
                    </ul>

                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>

@endsection
