<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!--fonts-->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/css/font-awesome.css">
    <!--//fonts-->

    <!-- Styles -->
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/css/lightbox.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="/css/app.css" rel="stylesheet">

    <script src="/js/jquery.min.js"></script>
    <script src="/js/lightbox.js"></script>

    <!-- Scripts -->
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="/js/responsiveslides.min.js"></script>
    <script>
        $(function () {
            $("#slider").responsiveSlides({
                auto: true,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                pager: true,
            });
        });
    </script>

</head>

<body>

<!--header-->
@include('partials._navbar')
<!---->

@if(Session::has('success'))
    <div class="container text-center">
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    </div>
@endif

@yield('content')

<div class="footer">
    <div class="footer-mid">
        <div class="container">

            <div class="col-md-3 grid1">
                <h3>Contacts</h3>
                <p>Dalai Islamic Center</p>
                <p>CDA Building, Kuu Street (Plot 18/3),</p>
                <p>P.O Box 2136, Dodoma.</p>
                <p>0262350299/0754305310</p>
                <p>dalaiislamiccenter@yahoo.com</p>
            </div>
            <div class="col-md-6 grid1">
                <h3>Newsletter</h3>
                <form>
                    <input type="text" value="" onfocus="this.value='';"
                           onblur="if (this.value == '') {this.value ='';}">
                    <input type="submit" value="SUBSCRIBE">
                </form>
            </div>
            <div class="col-md-3 grid2">
                <h3>Follow Us</h3>
                <ul class="social-in">
                    <li><a href="#"><i> </i></a></li>
                    <li><a href="#"><i class="twitter"> </i></a></li>
                    <li><a href="#"><i class="linked"> </i></a></li>
                    <li><a href="#"><i class="rss"> </i></a></li>
                    <li><a href="#"><i class="pin"> </i></a></li>
                </ul>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="container">
        <div class="class-footer">
            <p class="footer-grid">&copy; {{ date('Y') }} Dalai Islamic Center. All Rights Reserved</p>
        </div>
    </div>
</div>

<script src="/js/bootstrap.js"></script>

</body>
</html>
