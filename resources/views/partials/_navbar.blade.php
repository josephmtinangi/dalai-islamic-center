<div class="header">
    <div class="header-top">
        <div class="container">

            <div class="top-nav">
                <span class="menu"><img src="images/menu.png" alt=""> </span>

                <ul>
                    <li @if(Request::is('/')) class="active" @endif><a href="{{ url('/') }}">Home</a><span>/</span></li>
                    <li @if(Request::is('about')) class="active" @endif><a
                                href="{{ url('about') }}">About</a><span>/</span></li>
                    <li @if(Request::is('services')) class="active" @endif><a
                                href="{{ url('services') }}">Services</a><span>/</span></li>
                    <li @if(Request::is('gallery')) class="active" @endif><a
                                href="{{ url('gallery') }}">Gallery</a><span>/</span></li>
                    <li @if(Request::is('contact')) class="active" @endif><a href="{{ url('contact') }}">Contact</a>
                    </li>
                </ul>

                <!--script-->
                <script>
                    $("span.menu").click(function () {
                        $(".top-nav ul").slideToggle(500, function () {
                        });
                    });
                </script>
            </div>

        </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <div class="logo">
                <h1><a href="{{ url('/') }}">{{ config('app.name', 'Dalai Islamic Center') }}</a></h1>
                <p>Helping needy children</p>
            </div>
        </div>
    </div>
</div>