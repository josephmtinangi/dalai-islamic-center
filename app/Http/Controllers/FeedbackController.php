<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public function create(Request $request)
    {
        Feedback::create($request->all());

        $request->session()->flash('success', 'Thank you for your feedback!');

        return redirect('contact');
    }
}
